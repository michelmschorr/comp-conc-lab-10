# Computação Concorrente

## Laboratório 10

<br>

### Aluno:  Michel Monteiro Schorr<br>
### DRE:  120017379




<br><br>




## Estrutura
### Dentro da pasta lab, irá encontrar 3 pastas: 
**src**, contendo o condigo fonte principal <br>
**libs**, contendo as bibliotecas usadas e o arquivo include.h  <br>
**execs**, contendo os executaveis   <br>





<br><br>




## Comandos

Os comandos para compilar e executar são os seguintes:


<br>


``` 
gcc ./lab/src/lab.c -o ./lab/execs/lab -lpthread
./lab/execs/lab 3 2
```




<br><br>




## Exemplo:
``` 
[michelms@michel-pc1 lab10]$ ./lab/execs/lab
...
Thread 1 quer ler
Thread 2 quer ler
Thread 3 quer ler
Thread 5 terminou de escrever
Thread 4 escrevendo
Thread 4 terminou de escrever
Thread 4 liberando leitura
Thread 5 quer escrever
Thread 1 bloqueando escrita
Thread 1 lendo
Thread 2 lendo
Thread 3 lendo
Thread 5 bloqueando leitura
Thread 1 terminou de ler
Thread 4 quer escrever
Thread 2 terminou de ler
Thread 3 terminou de ler
Thread 3 liberando escrita
Thread 5 escrevendo
Thread 1 quer ler
Thread 5 terminou de escrever
Thread 2 quer ler
Thread 3 quer ler
Thread 4 escrevendo
...
[michelms@michel-pc1 lab10]$
``` 

/*
Computação Concorrente - Lab 10
Michel Monteiro Schorr
120017379
*/

#include "../libs/include.h"






//globais

sem_t em_e, em_l, escr, leit; //semaforos
int e=0, l=0; 
int L,E; //numero de threads leitoras e escritoras





void * leitor(void *arg){
    int id = *((int*)arg);

    printf("Thread %d leitora comecando\n", id);

    while(1) {
        printf("Thread %d quer ler\n", id);

        sem_wait(&leit);
        sem_wait(&em_l); l++;
        if(l==1){
            sem_wait(&escr);
            printf("Thread %d bloqueando escrita\n", id);
        }
        sem_post(&em_l);
        sem_post(&leit);

        //le...
        printf("Thread %d lendo\n", id);
        sleep(1);

        sem_wait(&em_l); l--;
        printf("Thread %d terminou de ler\n", id);
        if(l==0){
            printf("Thread %d liberando escrita\n", id);
            sem_post(&escr);
        }
        sem_post(&em_l);
        sleep(1);
    }

    pthread_exit(NULL);
}




void * escritor(void *arg){
    int id = *((int*)arg);

    printf("Thread %d escritora comecando\n", id);

    while(1) {
        printf("Thread %d quer escrever\n", id);

        sem_wait(&em_e); e++;
        if(e==1){
            sem_wait(&leit);
            printf("Thread %d bloqueando leitura\n", id);
        }
        sem_post(&em_e);
        sem_wait(&escr);

        //escreve...
        printf("Thread %d escrevendo\n", id);
        sleep(1);


        printf("Thread %d terminou de escrever\n", id);

        sem_post(&escr);

        sem_wait(&em_e); e--;
        if(e==0){
            printf("Thread %d liberando leitura\n", id);
            sem_post(&leit);
        }
        sem_post(&em_e);
        sleep(1);
    }

    pthread_exit(NULL);
}










int main(int argc, char **argv){

    //lendo o numero de threads
    if(argc < 3){
        printf("formato de chamada: \n  lab L(Numero de Threads Leitoras) E(Numero de Threads Escritoras)\n");
        exit(1);
    }

    L = atoi(argv[1]);
    printf("%d threads leitoras\n", L);
    E = atoi(argv[2]);
    printf("%d threads escritoras\n", E);


    int id[L+E]; 
    pthread_t threads[L+E];




    //inicializa os dois semaforos
    printf("Inicializando semaforos\n");
    sem_init(&em_e, 0, 1);
    sem_init(&em_l, 0, 1);
    sem_init(&escr, 0, 1);
    sem_init(&leit, 0, 1);



    //inicializa os ids
    printf("Inicializando os ids\n");
    for(int i =0; i< E+L; i++){
        id[i]= i+1;
    }


    /* Cria as threads */
    printf("Criando threads\n");
    for(int i =0; i<L; i++){
        pthread_create(&threads[i], NULL, leitor, &id[i]);
    }
    for(int i =L; i<L+E; i++){
        pthread_create(&threads[i], NULL, escritor, &id[i]);
    }





    /* Espera todas as threads completarem */
    printf("Dando join nas threads\n");
    for (int i = 0; i < E+L; i++) {
        pthread_join(threads[i], NULL);
    }



    /* Desaloca semaforos e termina */
    sem_destroy(&em_e);
    sem_destroy(&em_l);
    sem_destroy(&escr);
    sem_destroy(&leit);


    return 0;
}